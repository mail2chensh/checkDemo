//
//  AppDelegate.m
//  checkDemo
//
//  Created by dev on 15/10/31.
//  Copyright © 2015年 Chensh. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        [self something];
    }
    return self;
}

- (void)something {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"2009" ofType:@"txt"];
    
    NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
//    NSLog(@"%@", content);
    
    
    NSString *anStr = @" BCBDB 6CCCDB 11EEDAB 16EBBCD21EBBDB 26DDEEC 31EDDDC 36EEADD41EBCDC 46ECDEB 51EADBCA 56ABBD61CCDBC 66BCBED.";
    
//    NSCharacterSet *otherSet = [[NSCharacterSet characterSetWithCharactersInString:@"ABCEDFG"] invertedSet];
    anStr = [[anStr componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsJoinedByString:@""];
    anStr = [[anStr componentsSeparatedByCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]] componentsJoinedByString:@""];
    anStr = [[anStr componentsSeparatedByString:@"-"] componentsJoinedByString:@""];
    anStr = [[anStr componentsSeparatedByString:@"."] componentsJoinedByString:@""];
//    anStr = [anStr componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ];
    
    NSString *moreStr = @"AC、CD、DE、CD、DE、76 BCD、ABDE、CDE、CD、AE、81 ABD、AE、AD、BE、ABCDE、86 DE、ABC、ADE、CD、AD、91 ABD、AE、BC、DE、CDE、96 BC、ACD、BCD、BD、BCD";
    
    moreStr = [[moreStr componentsSeparatedByCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]] componentsJoinedByString:@""];
    
    NSArray *moreArr = [moreStr componentsSeparatedByString:@"、"];
    
    
    
    NSMutableArray *anArr = [NSMutableArray array];
    for (int i = 0; i < anStr.length; i++) {
        [anArr addObject:[anStr substringWithRange:NSMakeRange(i, 1)]];
    }
    
//    NSLog(@"anrr %ld", anArr.count);
   
    
    NSArray *array = [content componentsSeparatedByString:@"\n"];
    NSString *regex = @"^\\d.*";
    NSMutableArray *result = [NSMutableArray array];
    NSString *resultStr = @"";
    
    for (int i = 0, j = 0, z= 0; i < array.count; i++) {
        
        NSString *str = [array objectAtIndex:i];
        
        NSPredicate *predicte = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
        if (![predicte evaluateWithObject:str]) {
            resultStr = [NSString stringWithFormat:@"%@\n%@", resultStr, str];
            
        } else {
            
            if (![resultStr isEqualToString:@""]) {
                [result addObject:resultStr];
            }
            
            if (j < anArr.count) {
                resultStr = [NSString stringWithFormat:@"答案是【%@】 %@ ", anArr[j], str];
                j++;
            } else {
                if ( [self isPureInt:[str substringToIndex:1]] ) {
                    resultStr = [NSString stringWithFormat:@"答案是【 %@ 】 %@ ", moreArr[z], str];
                    z++;
                } else {
                    resultStr = [NSString stringWithFormat:@"答案是【 】 %@ ", str];

                }
            }
            
            
        }
    }
    
    [result addObject:resultStr];
    
    
    resultStr = @"";
    for (NSString *str in result) {
        resultStr = [NSString stringWithFormat:@"%@ \n\n %@", resultStr, str];
    }
    NSLog(@"result: %@", resultStr);
}


- (BOOL)isPureInt:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}


@end
